def LABEL_ID = "questcode-${UUID.randomUUID().toString()}"

podTemplate(
    name: 'questcode',
    namespace: 'devops',
    label: LABEL_ID, 
    containers: [
        containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'docker', name: 'docker-container', ttyEnabled: true, workingDir: '/home/jenkins/agent'),
        containerTemplate(args: 'cat', command: '/bin/sh -c', image: 'lachlanevenson/k8s-helm:v2.16.6', name: 'helm-container', ttyEnabled: 'true')
        ], 
         
    volumes: [hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')]
) 

{
    // START PIPELINE
    def REPOS
    def IMAGE_VERSION
    def IMAGE_POSFIX = ""
    def KUBE_NAMESPACE
    def ENVIRONMENT
    def IMAGE_NAME = "backend-scm"
    def GIT_BRANCH
    def HELM_CHART_NAME = "questcode/backend-scm"
    def HELM_DEPLOY_NAME
    def CHARTMUSEUM_URL = "http://helm-chartmuseum.devops:8080"
    def INGRESS_HOST = "questcode.org"
    
    node(LABEL_ID) {
        //Checkout e ENVs
        stage('Checkout') {
            echo 'Iniciando clone do repositorio'
            REPOS = checkout scm
            GIT_BRANCH = REPOS.GIT_BRANCH
            if(GIT_BRANCH.equals("master")) {
                KUBE_NAMESPACE = "prod"
                ENVIRONMENT = "production"
                HELM_DEPLOY_NAME = KUBE_NAMESPACE + "-${IMAGE_NAME}"
            } else if(GIT_BRANCH.equals("develop")) {
                KUBE_NAMESPACE = "staging"
                ENVIRONMENT = "staging"
                IMAGE_POSFIX = "-RC"
                HELM_DEPLOY_NAME = KUBE_NAMESPACE + "-${IMAGE_NAME}"
                INGRESS_HOST = ENVIRONMENT + ".questcode.org"
            } else {
                def error = "Nao existe pipeline para a branch ${GIT_BRANCH}"
                echo error
                throw new Exception(error)            }
            IMAGE_VERSION = sh label: '', returnStdout: true, script: 'sh read-package-version.sh'
            IMAGE_VERSION = IMAGE_VERSION.trim() + IMAGE_POSFIX
        }
        stage('Package') {
            container('docker-container') {
                echo 'Iniciando empacotamento com Docker'
                withCredentials([usernamePassword(credentialsId: 'dockerhub', passwordVariable: 'DOCKER_HUB_PASSWORD', usernameVariable: 'DOCKER_HUB_USER')]) {
                    sh "docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}"
                    sh "docker build -t ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION} ."
                    sh "docker push ${DOCKER_HUB_USER}/${IMAGE_NAME}:${IMAGE_VERSION}"
                }
            }
            
        }
        stage('Deploy') {
            container('helm-container'){
                echo 'Iniciando deploy com Helm!'
                sh """
                    helm init --client-only
                    helm repo add questcode ${CHARTMUSEUM_URL}
                    helm repo update
                """
                try{
                        //Fazer helm upgrade
                        sh "helm upgrade --namespace=${KUBE_NAMESPACE} ${HELM_DEPLOY_NAME} ${HELM_CHART_NAME} --set image.tag=${IMAGE_VERSION} --set ingress.hosts[0]=${INGRESS_HOST}"
                }    catch (Exception e) {
                        //Fazer helm install
                        sh "helm install --namespace=${KUBE_NAMESPACE} --name ${HELM_DEPLOY_NAME} ${HELM_CHART_NAME} --set image.tag=${IMAGE_VERSION} --set ingress.hosts[0]=${INGRESS_HOST}"
                }
            }
        }
        
    }  // end of node
}